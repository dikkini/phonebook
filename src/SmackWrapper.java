import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

/**
 * Created with IntelliJ IDEA.
 * User: haribo
 * Date: 24.03.13
 * Time: 23:09
 */
public class SmackWrapper implements SmackWrapperInterface {

    public Connection ConnectToServerPort(String serverAdress, Integer port)
            throws XMPPException {
        ConnectionConfiguration config = new ConnectionConfiguration(serverAdress, port);
        Connection connection = new XMPPConnection(config);
        connection.connect();
        return connection;
    }

    public Connection ConnectToServerPort(String serverAdress)
            throws XMPPException {
        ConnectionConfiguration config = new ConnectionConfiguration(serverAdress, 5222);
        Connection connection = new XMPPConnection(config);
        connection.connect();
        return connection;
    }

    public Connection ConnectToServerPort(String serverAdress, Integer port,
                                          Boolean compressionEnable, Boolean authSASLEnable)
            throws XMPPException {
        ConnectionConfiguration config = new ConnectionConfiguration(serverAdress, port);
        config.setCompressionEnabled(compressionEnable);
        config.setSASLAuthenticationEnabled(authSASLEnable);
        Connection connection = new XMPPConnection(config);
        connection.connect();
        return connection;
    }

    public void LogIn(Connection connection, String login, String password, String resource) throws XMPPException {
        connection.connect();
        connection.login(login, password, resource);
    }

    public void Disconnect(Connection connection) {
        connection.disconnect();
    }
}
