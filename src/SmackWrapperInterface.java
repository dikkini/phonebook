import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPException;

/**
 * Created with IntelliJ IDEA.
 * User: haribo
 * Date: 24.03.13
 * Time: 23:57
 */
public interface SmackWrapperInterface {

    /**
     * Connect to the server with specified port.
     * @param serverAdress server adress, for a example: "jabber.org"
     * @param port server port:
     * @return Connection object.
     * @throws XMPPException
     */
    Connection ConnectToServerPort(String serverAdress, Integer port) throws XMPPException;

    /**
     * Connect to the server with standart port: 5222.
     * @param serverAdress server adress, for a example: "jabber.org"
     * @return Connection object.
     * @throws XMPPException
     */
    Connection ConnectToServerPort(String serverAdress) throws XMPPException;

    /**
     * Connect to the server with specified port and addition options.
     * @param serverAdress Server adress, for a example: "jabber.org"
     * @param port Server port
     * @param compressionEnable Booelan value for enable (true) or disable (false) compression traffic to the server
     * @param authSASLEnable Booelan value for enable (true) or disable (false) SASL Authentication.
     * @return Connection Object.
     * @throws XMPPException
     */
    Connection ConnectToServerPort(String serverAdress, Integer port, Boolean compressionEnable, Boolean authSASLEnable)
            throws XMPPException;

    /**
     * Login to the server
     * @param connection Connection object
     * @param login Login to the server
     * @param password Password
     * @param resource Someresource (?) Dont know what it could be.
     */
    void LogIn(Connection connection, String login, String password, String resource) throws XMPPException;

    /**
     * Disconnect from the server.
     * @param connection Opened connection object.
     */
    void Disconnect(Connection connection);

}
