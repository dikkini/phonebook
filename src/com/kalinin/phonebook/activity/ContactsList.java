package com.kalinin.phonebook.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.view.View;
import android.widget.*;
import com.kalinin.phonebook.R;
import com.kalinin.phonebook.activity.model.ContactModel;

import java.util.ArrayList;


public class ContactsList extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ListView contactListView = (ListView) findViewById(R.id.contact_list);
        CheckBox checkAll = (CheckBox) findViewById(R.id.check_all_box);
        TextView statusMoneyTxt = (TextView) findViewById(R.id.status_money);
        Button invite = (Button) findViewById(R.id.ok_btn);

        statusMoneyTxt.setText("0");
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {
                ListView contactListView = (ListView) findViewById(R.id.contact_list);
                setMoneyText(contactListView);
            }
        });

        checkAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ListView contactListView = (ListView) findViewById(R.id.contact_list);
                ListAdapter adapter = contactListView.getAdapter();
                for (int i = 0; i < adapter.getCount(); i++) {
                    contactListView.setItemChecked(i, isChecked);
                }
                setMoneyText(contactListView);
            }
        });

        invite.setOnClickListener(new CompoundButton.OnClickListener() {

            @Override
            public void onClick(View view) {
                ListView contactListView = (ListView) findViewById(R.id.contact_list);
                long[] checkItemIds = contactListView.getCheckItemIds();
                ArrayList<ContactModel> checkedContactList = new ArrayList<>();
                for (long checkItemId : checkItemIds) {
                    ContactModel itemAtPosition = (ContactModel) contactListView.getItemAtPosition(
                            new Integer(String.valueOf(checkItemId)));
                    checkedContactList.add(itemAtPosition);
                }
                for (ContactModel contactModel : checkedContactList) {
                    // TODO собрать телефонные номера и отправить по jabber
                    System.out.println(contactModel.getPhonenumber());
                }
            }
        });

        final ArrayList<ContactModel> contactsList = getContacts();

        ArrayAdapter<ContactModel> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_multiple_choice, contactsList);

        contactListView.setAdapter(adapter);
    }

    private void setMoneyText(ListView contactListView) {
        TextView statusMoneyTxt = (TextView) findViewById(R.id.status_money);
        statusMoneyTxt.setText(" ");
        long[] checkedItemIds = contactListView.getCheckItemIds();
        Integer countMoney = checkedItemIds.length * 30;
        statusMoneyTxt.setText(countMoney.toString());
    }

    public ArrayList<ContactModel> getContacts() {
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{Phone._ID, Phone.DISPLAY_NAME, Phone.NUMBER}, null, null, null);

        startManagingCursor(cursor);
        ArrayList<ContactModel> contactsList = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String hasPhone = cursor.getString(2);
                if (hasPhone.length() > 1) {
                    contactsList.add(new ContactModel(cursor.getString(1), cursor.getString(2)));
                }
            }
        }
        return contactsList;
    }
}