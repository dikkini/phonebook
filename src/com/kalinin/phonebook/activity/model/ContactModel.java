package com.kalinin.phonebook.activity.model;

/**
 * Created with IntelliJ IDEA.
 * User: haribo
 * Date: 24.03.13
 * Time: 2:41
 */
public class ContactModel {
    private String name = "" ;
    private String phonenumber;
    private boolean checked = false ;

    public ContactModel(String name, String phonenumber) {
        this.name = name ;
        this.phonenumber = phonenumber;
    }
    public ContactModel(String name, String phonenumber, boolean checked) {
        this.name = name ;
        this.checked = checked ;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public boolean isChecked() {
        return checked;
    }
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    public String toString() {
        return name ;
    }
    public void toggleChecked() {
        checked = !checked ;
    }
}